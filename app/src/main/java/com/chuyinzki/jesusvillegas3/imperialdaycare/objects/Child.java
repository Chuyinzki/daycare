package com.chuyinzki.jesusvillegas3.imperialdaycare.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jesus Villegas 3 on 10/24/2016.
 */

public class Child implements Parcelable {

    private Long id;
    private String firstName;
    private String lastName;
    private String famId;
    private String childId;
    private String parentName;
    private String techNumber;
    private String caseNumber;
    private Boolean activeStatus;
    private Long dob;

    public Child(Long id, String firstName, String lastName, String famId, String childId,
                 String parentName, String techNumber, String caseNumber, Boolean activeStatus, Long dob){
        this.id = id;
        this.firstName = firstName.trim();
        this.lastName = lastName.trim();
        this.famId = famId.trim();
        this.childId = childId.trim();
        this.parentName = parentName.trim();
        this.techNumber = techNumber.trim();
        this.caseNumber = caseNumber != null ? caseNumber.trim() : "";
        this.activeStatus = activeStatus;
        this.dob = dob;
    }
    public Child(){
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeLong(id);
        dest.writeStringArray(new String[] {firstName, lastName, famId, childId, parentName,
                techNumber, caseNumber});
        dest.writeByte((byte) (activeStatus ? 1 : 0));
        dest.writeLong(dob);
    }

    public Child(Parcel in){
        this.id = in.readLong();
        String[] data = new String[7];
        in.readStringArray(data);
        this.firstName = data[0];
        this.lastName = data[1];
        this.famId = data[2];
        this.childId = data[3];
        this.parentName = data[4];
        this.techNumber = data[5];
        this.caseNumber = data[6];
        this.activeStatus = in.readByte() != 0;
        this.dob = in.readLong();
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFamId(String famId) {
        this.famId = famId;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFamId() {
        return famId;
    }

    public String getParentName() {
        return parentName;
    }

    public String getTechNumber() {
        return techNumber;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public void setTechNumber(String techNumber) {
        this.techNumber = techNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    public boolean getActiveStatus() {
        return activeStatus == null || activeStatus;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Long getDob() {
        return dob;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Child && id.equals(((Child) o).getId());
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Child createFromParcel(Parcel in) {
            return new Child(in);
        }

        public Child[] newArray(int size) {
            return new Child[size];
        }
    };
}
