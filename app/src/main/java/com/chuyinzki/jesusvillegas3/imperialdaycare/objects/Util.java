package com.chuyinzki.jesusvillegas3.imperialdaycare.objects;

import android.content.Context;
import android.os.Build;
import android.util.Pair;

import com.chuyinzki.jesusvillegas3.imperialdaycare.ChildDatabase;

import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Jesus Villegas 3 on 11/22/2016.
 */

public class Util {

    public static Long getCurrentTimeMillis() {
        Calendar cal = Calendar.getInstance();
        return cal.getTimeInMillis();
    }

    public static String getMonthDayYear(String delimiter) {
        return getMonthDayYear(delimiter, new Date());
    }

    public static String getMonthDayYear(String delimiter, Date date) {
        delimiter = delimiter == null ? "" : delimiter;
        return new SimpleDateFormat(String.format("MM%sdd%syyyy", delimiter, delimiter), Locale.US)
                .format(date);
    }

    public static String getTimeInHuman(Long time) {
        return new SimpleDateFormat("hh:mm a", Locale.US).format(new Date(time));
    }

    public static String getTimeInHuman(String time) {
        if(time == null) {
            return "";
        }
        return getTimeInHuman(Long.parseLong(time));
    }

    public static String getHourIn24(Long time) {
        return new SimpleDateFormat("HH", Locale.US).format(new Date(time));
    }

    public static String getMinutes(Long time) {
        return getTimeInHuman(time).substring(3, 5);
    }

    public static String getAge(long timeInMillis) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTimeInMillis(timeInMillis);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        return Integer.toString(age);
    }

    public static boolean allRecordsCompleteAndValid(List<DayRecord> records) {
        for(DayRecord record: records)
            if(!record.isCompleteAndValid())
                return false;
        return true;
    }

    public static void fillOutEmptyRecords(List<DayRecord> records) {
        DayRecord tempRecord = records.get(0);
        int year = Integer.parseInt(tempRecord.getFullYear());
        int month = Integer.parseInt(tempRecord.getMonth());
        int daysInMonth = tempRecord.getDaysInCurrentMonth();

        for(int i = 1; i <= daysInMonth; i++) {
            boolean found = false;
            for(DayRecord record : records) {
                if(record.getDayAsCalendar().get(Calendar.DAY_OF_MONTH) == i) {
                    found = true;
                    break;
                }
            }
            if(!found) {
                records.add(new DayRecord(getMonthDayYear(null, new Date(year - 1900, month - 1, i))));
            }
        }

    }

    public static List<DayRecord> getRecordsForWeekNumber(List<DayRecord> records, int week) {
        ArrayList<DayRecord> ret = new ArrayList<>();
        for (DayRecord record : records)
            if (record.getDayAsCalendar().get(Calendar.WEEK_OF_MONTH) == week)
                ret.add(record);
        Collections.sort(ret);
        return ret;
    }

    //First is 6AM - 6PM Monday - Friday, Second is after 6PM and Weekends
    //This function only supports getting am pm hours with school that is only in the am time.
    //TODO: Let user know that hours for school are only supported for am times
    public static Pair<Float, Float> getAmPmHours(DayRecord record) {
        if (!record.isCompleteAndValid())
            return new Pair<>(0f, 0f);

        Calendar checkIn = Calendar.getInstance();
        Calendar checkOut = Calendar.getInstance();
        checkIn.setTimeInMillis(Long.parseLong(record.getCheckIn()));
        checkOut.setTimeInMillis(Long.parseLong(record.getCheckOut()));

        Calendar cal6AM = Calendar.getInstance();
        cal6AM.setTimeInMillis(Long.parseLong(record.getCheckIn()));
        cal6AM.clear(Calendar.HOUR_OF_DAY);
        cal6AM.clear(Calendar.MINUTE);
        cal6AM.set(Calendar.HOUR_OF_DAY, 6);

        Calendar cal6PM = Calendar.getInstance();
        cal6PM.setTimeInMillis(Long.parseLong(record.getCheckIn()));
        cal6PM.clear(Calendar.HOUR_OF_DAY);
        cal6PM.clear(Calendar.MINUTE);
        cal6PM.set(Calendar.HOUR_OF_DAY, 18);

        int dayOfWeek = checkIn.get(Calendar.DAY_OF_WEEK);
        //If it's the weekend or they checked in after 6pm
        if (dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.SATURDAY || checkIn.compareTo(cal6PM) >= 0) {
            Float diffInHours = millisToHours(checkOut.getTimeInMillis() - checkIn.getTimeInMillis());
            Float schoolDiffInHours =  millisToHours(getSchoolMillis(record));
            return new Pair<>(0f, diffInHours - schoolDiffInHours);
        }

        long pmMillis = 0;
        if (checkIn.compareTo(cal6AM) <= 0) {
            pmMillis += checkOut.compareTo(cal6AM) <= 0 ? checkOut.getTimeInMillis() : cal6AM.getTimeInMillis() -
                    checkIn.getTimeInMillis();
        }
        long amMillis =
                (checkOut.compareTo(cal6PM) <= 0 ? checkOut.getTimeInMillis() : cal6PM.getTimeInMillis()) -
                        (checkIn.compareTo(cal6AM) <= 0 ? cal6AM.getTimeInMillis() : checkIn.getTimeInMillis());
        if (checkOut.compareTo(cal6PM) >= 0) {
            pmMillis += checkOut.getTimeInMillis() - cal6PM.getTimeInMillis();
        }

        return new Pair<>(millisToHours(amMillis - getSchoolMillis(record)), millisToHours(pmMillis));
    }

    public static long getSchoolMillis(DayRecord record) {
        return record.hasSchoolHours() ? Long.parseLong(record.getSchoolCheckIn()) -
                Long.parseLong(record.getSchoolCheckOut()) : 0;
    }

    public static Pair<Float, Float> getAmPmHoursForWeek(List<DayRecord> records, int week) {
        return getAllAmPmHours(getRecordsForWeekNumber(records, week));
    }

    public static Pair<Float, Float> getAllAmPmHours(List<DayRecord> records) {
        Float amRunning = 0f;
        Float pmRunning = 0f;
        for (DayRecord record : records) {
            Pair<Float, Float> curAmPm = getAmPmHours(record);
            amRunning += curAmPm.first;
            pmRunning += curAmPm.second;
        }
        return new Pair<>(amRunning, pmRunning);
    }

    public static float millisToHours(long millis) {
        return millis / (1000f * 60 * 60);
    }
}
