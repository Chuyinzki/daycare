package com.chuyinzki.jesusvillegas3.imperialdaycare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.chuyinzki.jesusvillegas3.imperialdaycare.ChildDatabase;
import com.chuyinzki.jesusvillegas3.imperialdaycare.DatePickerFragment;
import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.Child;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChildAdderActivity extends AppCompatActivity {

    static final String CHILD_EXTRA_RESULT = "CHILD_EXTRA_RESULT";
    static final String CHILD_EXTRA_DELETED = "CHILD_EXTRA_DELETED";
    static final int CHILD_EDIT_MODE = 103;

    @BindView(R.id.dob_button)
    Button dobButton;
    @BindView(R.id.first_name_et)
    EditText fNameET;
    @BindView(R.id.last_name_et)
    EditText lNameET;
    @BindView(R.id.family_id_et)
    EditText fIdET;
    @BindView(R.id.child_id_et)
    EditText childIdET;
    @BindView(R.id.parent_name_et)
    EditText pNameET;
    @BindView(R.id.tech_number_et)
    EditText techNumberET;
    @BindView(R.id.case_number_et)
    EditText caseNumberET;
    @BindView(R.id.active_checkBox)
    CheckBox activeCheckbox;

    int year = -1;
    int month = -1;
    int day = -1;

    Child currentChild;
    int mode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.child_adder);
        ButterKnife.bind(this);

        Intent i = getIntent();
        currentChild = i.getParcelableExtra(MainActivity.CHILD_EXTRA);
        if (currentChild != null) {
            mode = CHILD_EDIT_MODE;
            fNameET.setText(currentChild.getFirstName());
            lNameET.setText(currentChild.getLastName());
            fIdET.setText(currentChild.getFamId());
            childIdET.setText(currentChild.getChildId());
            pNameET.setText(currentChild.getParentName());
            techNumberET.setText(currentChild.getTechNumber());
            caseNumberET.setText(currentChild.getCaseNumber());
            activeCheckbox.setChecked(currentChild.getActiveStatus());
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(currentChild.getDob());
            setMonthDayYear(cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR));
        } else {
            currentChild = new Child();
            activeCheckbox.setChecked(true);
        }
        setButtonDate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (mode == CHILD_EDIT_MODE)
            getMenuInflater().inflate(R.menu.child_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.delete_child) {
            //Todo: Use modal pattern in the future
            boolean result = ChildDatabase.getInstance(this).deleteChild(currentChild);
            Toast.makeText(this, result ? R.string.child_delete_success : R.string.something_wrong, Toast.LENGTH_SHORT).show();
            Intent intent = this.getIntent();
            intent.putExtra(CHILD_EXTRA_RESULT, currentChild);
            intent.putExtra(CHILD_EXTRA_DELETED, true);
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setMonthDayYear(int month, int day, int year) {
        this.month = month;
        this.day = day;
        this.year = year;
    }

    public void showDatePickerDialog(View v) {
        DatePickerFragment dateFragment = new DatePickerFragment();
        dateFragment.setMonthDayYear(month, day, year);
        dateFragment.show(getFragmentManager(), "datePicker");
    }

    public void saveChildAccount(View v) {
        String firstName = fNameET.getText().toString().trim();
        String lastName = lNameET.getText().toString().trim();
        String famId = fIdET.getText().toString().trim();
        String childId = childIdET.getText().toString().trim();
        String parentName = pNameET.getText().toString().trim();
        String techNumber = techNumberET.getText().toString().trim();
        String caseNumber = caseNumberET.getText().toString().trim();
        boolean activeStatus = activeCheckbox.isChecked();
        Calendar dobCal = Calendar.getInstance();
        dobCal.set(year, month, day);

        currentChild.setFirstName(firstName);
        currentChild.setLastName(lastName);
        currentChild.setFamId(famId);
        currentChild.setChildId(childId);
        currentChild.setParentName(parentName);
        currentChild.setTechNumber(techNumber);
        currentChild.setCaseNumber(caseNumber);
        currentChild.setActiveStatus(activeStatus);
        currentChild.setDob(dobCal.getTimeInMillis());

        if (firstName.isEmpty() || lastName.isEmpty() || famId.isEmpty() || childId.isEmpty() || parentName.isEmpty()) {
            Toast.makeText(this, R.string.error_missing_fields, Toast.LENGTH_SHORT).show();
            return;
        } else if (dobCal.compareTo(Calendar.getInstance()) > 0) {
            Toast.makeText(this, R.string.error_invalid_dob, Toast.LENGTH_SHORT).show();
            return;
        } else if (mode != CHILD_EDIT_MODE && ChildDatabase.getInstance(this).childIdExists(currentChild)) {
            Toast.makeText(this, R.string.error_duplicate_child_id, Toast.LENGTH_SHORT).show();
            return;
        }

        if (mode != CHILD_EDIT_MODE) {
            currentChild = new Child(Calendar.getInstance().getTimeInMillis(),
                    firstName, lastName, famId, childId, parentName, techNumber,
                    caseNumber, activeStatus, dobCal.getTimeInMillis());
            ChildDatabase.getInstance(this).insertChild(currentChild);
        } else
            ChildDatabase.getInstance(this).updateChild(currentChild);
        ChildDatabase.getInstance(this).logChildren();
        Toast.makeText(this, mode != CHILD_EDIT_MODE ? R.string.child_add_success :
                R.string.child_update_success, Toast.LENGTH_SHORT).show();

        Intent intent = this.getIntent();
        intent.putExtra(CHILD_EXTRA_RESULT, currentChild);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void setButtonDate() {
        if (month == -1 && day == -1 && year == -1) {
            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR) - 3;
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }
        dobButton.setText(String.format(Locale.US, "%d/%d/%d", month + 1, day, year));
    }
}
