package com.chuyinzki.jesusvillegas3.imperialdaycare.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.chuyinzki.jesusvillegas3.imperialdaycare.ChildDatabase;
import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.Child;
import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.DayRecord;
import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.Util;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.chuyinzki.jesusvillegas3.imperialdaycare.activities.MainActivity.CHILD_EXTRA;
import static com.chuyinzki.jesusvillegas3.imperialdaycare.activities.MainActivity.RECORD_EXTRA;

/**
 * Created by Jesus Villegas 3 on 3/14/2018.
 */

public class RecordModifyActivity extends ParentMainActivity {
    Child child;
    DayRecord dayRecord;

    @BindView(R.id.date_button)
    Button dateButton;
    @BindView(R.id.checkin_button)
    Button checkinButton;
    @BindView(R.id.checkout_button)
    Button checkoutButton;
    @BindView(R.id.school_checkin_button)
    Button schoolCheckinButton;
    @BindView(R.id.school_checkout_button)
    Button schoolCheckoutButton;
    @BindView(R.id.save_button)
    Button saveButton;
    @BindView(R.id.delete_button)
    Button deleteButton;

    static Calendar dateCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_record_editor_layout);
        ButterKnife.bind(this);

        Intent i = getIntent();
        child = i.getParcelableExtra(CHILD_EXTRA);
        dayRecord = i.getParcelableExtra(RECORD_EXTRA);

        updateDateTime();

        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(RecordModifyActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                dateCalendar.set(Calendar.YEAR, year);
                                dateCalendar.set(Calendar.MONTH, monthOfYear);
                                dateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                dayRecord.setTimeID(Util.getMonthDayYear("", dateCalendar.getTime()));
                                updateDateTime();
                            }
                        }, dateCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH),
                        dateCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        checkinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(RecordModifyActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                dateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                dateCalendar.set(Calendar.MINUTE, minute);
                                dayRecord.setCheckIn(String.valueOf(dateCalendar.getTimeInMillis()));
                                updateDateTime();
                            }
                        },
                        dayRecord.getCheckIn() != null ? Integer.parseInt(Util.getHourIn24(Long.parseLong(dayRecord.getCheckIn()))) : 8,
                        dayRecord.getCheckIn() != null ? Integer.parseInt(Util.getMinutes(Long.parseLong(dayRecord.getCheckIn()))) : 30,
                        DateFormat.is24HourFormat(RecordModifyActivity.this)).show();
            }
        });

        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(RecordModifyActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                dateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                dateCalendar.set(Calendar.MINUTE, minute);
                                dayRecord.setCheckOut(String.valueOf(dateCalendar.getTimeInMillis()));
                                updateDateTime();
                            }
                        },
                        dayRecord.getCheckOut() != null ? Integer.parseInt(Util.getHourIn24(Long.parseLong(dayRecord.getCheckOut()))) : 17,
                        dayRecord.getCheckOut() != null ? Integer.parseInt(Util.getMinutes(Long.parseLong(dayRecord.getCheckOut()))) : 30,
                        DateFormat.is24HourFormat(RecordModifyActivity.this)).show();
            }
        });

        schoolCheckoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(RecordModifyActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                dateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                dateCalendar.set(Calendar.MINUTE, minute);
                                dayRecord.setSchoolCheckOut(String.valueOf(dateCalendar.getTimeInMillis()));
                                updateDateTime();
                            }
                        },
                        dayRecord.getSchoolCheckOut() != null ? Integer.parseInt(Util.getHourIn24(Long.parseLong(dayRecord.getSchoolCheckOut()))) : 11,
                        dayRecord.getSchoolCheckOut() != null ? Integer.parseInt(Util.getMinutes(Long.parseLong(dayRecord.getSchoolCheckOut()))) : 30,
                        DateFormat.is24HourFormat(RecordModifyActivity.this)).show();
            }
        });
        schoolCheckinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(RecordModifyActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                dateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                dateCalendar.set(Calendar.MINUTE, minute);
                                dayRecord.setSchoolCheckIn(String.valueOf(dateCalendar.getTimeInMillis()));
                                updateDateTime();
                            }
                        },
                        dayRecord.getSchoolCheckIn() != null ? Integer.parseInt(Util.getHourIn24(Long.parseLong(dayRecord.getSchoolCheckIn()))) : 14,
                        dayRecord.getSchoolCheckIn() != null ? Integer.parseInt(Util.getMinutes(Long.parseLong(dayRecord.getSchoolCheckIn()))) : 30,
                        DateFormat.is24HourFormat(RecordModifyActivity.this)).show();
            }
        });


        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dayRecord.getTimeID() == null) {
                    Toast.makeText(RecordModifyActivity.this,
                            "Something went wrong. Time ID is null.", Toast.LENGTH_LONG).show();
                    return;
                }

                //normalizing times to the correct possibly changed day
                if (dayRecord.getCheckIn() != null) {
                    setDateCalendar(dayRecord.getCheckIn());
                    dayRecord.setCheckIn(String.valueOf(dateCalendar.getTimeInMillis()));
                }

                if (dayRecord.getCheckOut() != null) {
                    setDateCalendar(dayRecord.getCheckOut());
                    dayRecord.setCheckOut(String.valueOf(dateCalendar.getTimeInMillis()));
                }

                if (dayRecord.getSchoolCheckIn() != null) {
                    setDateCalendar(dayRecord.getSchoolCheckIn());
                    dayRecord.setSchoolCheckIn(String.valueOf(dateCalendar.getTimeInMillis()));
                }

                if (dayRecord.getSchoolCheckOut() != null) {
                    setDateCalendar(dayRecord.getSchoolCheckOut());
                    dayRecord.setSchoolCheckOut(String.valueOf(dateCalendar.getTimeInMillis()));
                }

                if (dayRecord.getCheckIn() != null && dayRecord.getCheckOut() != null &&
                        dayRecord.getCheckIn().compareTo(dayRecord.getCheckOut()) > 0) {
                    Toast.makeText(RecordModifyActivity.this,
                            "Cannot have checked in after checking out.", Toast.LENGTH_LONG).show();
                    return;
                }

                if (dayRecord.getSchoolCheckIn() != null && dayRecord.getSchoolCheckOut() != null &&
                        dayRecord.getSchoolCheckOut().compareTo(dayRecord.getSchoolCheckIn()) > 0) {
                    Toast.makeText(RecordModifyActivity.this,
                            "Cannot have checked in from school before checking out for school.", Toast.LENGTH_LONG).show();
                    return;
                }

                if (!dayRecord.isCurrentlyValid()) {
                    Toast.makeText(RecordModifyActivity.this,
                            "These values are not valid. Please fix.", Toast.LENGTH_LONG).show();
                    return;
                }

                ChildDatabase.getInstance(RecordModifyActivity.this).insertOrUpdateDateTimes(child, dayRecord);
                finish();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean deleted = ChildDatabase.getInstance(RecordModifyActivity.this).deleteDateTime(child, dayRecord);
                Toast.makeText(RecordModifyActivity.this,
                        String.format("Date deletion %s.", deleted ? "successful": "failed"), Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    private static void setDateCalendar(String millis) {
        dateCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(Util.getHourIn24(Long.parseLong(millis))));
        dateCalendar.set(Calendar.MINUTE, Integer.parseInt(Util.getMinutes(Long.parseLong(millis))));
    }

    private void updateDateTime() {
        if (dayRecord != null) {
            if (dayRecord.getTimeID() != null) {
                dateButton.setText(dayRecord.getFriendlyFormattedDate());
                dateCalendar.set(Integer.parseInt(dayRecord.getFullYear()),
                        Integer.parseInt(dayRecord.getMonth()) - 1,
                        Integer.parseInt(dayRecord.getDay()));
            }
            if (dayRecord.getCheckIn() != null)
                checkinButton.setText(Util.getTimeInHuman(Long.valueOf(dayRecord.getCheckIn())));
            if (dayRecord.getCheckOut() != null)
                checkoutButton.setText(Util.getTimeInHuman(Long.valueOf(dayRecord.getCheckOut())));
            if (dayRecord.getSchoolCheckIn() != null)
                schoolCheckinButton.setText(Util.getTimeInHuman(Long.valueOf(dayRecord.getSchoolCheckIn())));
            if (dayRecord.getSchoolCheckOut() != null)
                schoolCheckoutButton.setText(Util.getTimeInHuman(Long.valueOf(dayRecord.getSchoolCheckOut())));
        }
    }
}
