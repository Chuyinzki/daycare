package com.chuyinzki.jesusvillegas3.imperialdaycare;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;

import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.Child;
import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.DayRecord;
import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.Util;

import java.time.YearMonth;
import java.util.Calendar;

import static java.util.Calendar.SATURDAY;
import static java.util.Calendar.SUNDAY;

public class Utils {
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public static void debugAddValuesForMonth(Child currentChild, Context context, String formattedDate,
                                              int checkInHour, Integer schoolCheckOutHour, Integer schoolCheckInHour, int checkOutHour) {
        int year = Integer.parseInt(formattedDate.substring(4));
        int month = Integer.parseInt(formattedDate.substring(0,2));

        Calendar tempCal = Calendar.getInstance();
        tempCal.set(year, month - 1, 15);
        int daysInMonth = tempCal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for(int i = 1; i <= daysInMonth; i++) {
            Calendar cal = Calendar.getInstance();
            cal.set(year, month - 1, i, checkInHour, 0);
            int day = cal.get(Calendar.DAY_OF_WEEK);
            if(day == SUNDAY || day == SATURDAY)
                continue;
            DayRecord record = new DayRecord(Util.getMonthDayYear(null, cal.getTime()));
            record.setCheckIn(cal.getTimeInMillis());
            cal.set(year,month - 1, i, checkOutHour,0);
            record.setCheckOut(cal.getTimeInMillis());

            if(schoolCheckInHour != null) {
                cal.set(year, month - 1, i, schoolCheckInHour, 0);
                record.setSchoolCheckIn(cal.getTimeInMillis());
            }

            if(schoolCheckOutHour != null) {
                cal.set(year, month - 1, i, schoolCheckOutHour, 0);
                record.setSchoolCheckOut(cal.getTimeInMillis());
            }

            ChildDatabase.getInstance(context).insertOrUpdateDateTimes(currentChild, record);
        }
    }
}
