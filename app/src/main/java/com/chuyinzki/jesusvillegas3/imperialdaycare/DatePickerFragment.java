package com.chuyinzki.jesusvillegas3.imperialdaycare;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import com.chuyinzki.jesusvillegas3.imperialdaycare.activities.ChildAdderActivity;

import java.util.Calendar;

/**
 * Created by Jesus Villegas 3 on 10/23/2016.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    int year = -1;
    int month = -1;
    int day = -1;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        if (year == -1) {
            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR) - 3;
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void setMonthDayYear(int month, int day, int year) {
        this.month = month;
        this.day = day;
        this.year = year;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (getActivity() instanceof ChildAdderActivity) {
            ((ChildAdderActivity) getActivity()).setMonthDayYear(month, day, year);
            ((ChildAdderActivity) getActivity()).setButtonDate();
        }
    }
}
