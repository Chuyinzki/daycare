package com.chuyinzki.jesusvillegas3.imperialdaycare;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Pair;

/**
 * Created by Jesus Villegas 3 on 10/24/2016.
 */

public class ProviderPreferences {
    private final static String PROVIDER_PREFERENCES = "PROVIDER_PREFERENCES";
    private final static String PROVIDER_NAME_KEY = "PROVIDER_NAME_KEY";
    private final static String PROVIDER_ID_KEY = "PROVIDER_ID_KEY";
    private final static String PROVIDER_ALT_ID_KEY = "PROVIDER_ALT_ID_KEY";

    public static void setProviderInfo(String name, String key, String altId, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PROVIDER_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROVIDER_NAME_KEY, name)
                .putString(PROVIDER_ID_KEY, key)
                .putString(PROVIDER_ALT_ID_KEY, altId)
                .apply();
    }

    public static boolean preferencesValid(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PROVIDER_PREFERENCES, Context.MODE_PRIVATE);
        return prefs.getString(PROVIDER_NAME_KEY, null) != null && prefs.getString(PROVIDER_ID_KEY, null) != null;
    }

    public static Preferences getProviderInfo(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PROVIDER_PREFERENCES, Context.MODE_PRIVATE);
        return preferencesValid(context) ?
                new Preferences(prefs.getString(PROVIDER_NAME_KEY, null),
                        prefs.getString(PROVIDER_ID_KEY, null),
                        prefs.getString(PROVIDER_ALT_ID_KEY, null)) : null;
    }

    public static class Preferences {
        public final String name, key, altId;

        public Preferences(String name, String key, String altId) {
            this.name = name;
            this.key = key;
            this.altId = altId;
        }
    }
}
