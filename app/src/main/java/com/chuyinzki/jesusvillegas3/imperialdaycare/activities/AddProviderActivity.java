package com.chuyinzki.jesusvillegas3.imperialdaycare.activities;

import android.os.Bundle;
import android.util.Pair;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.chuyinzki.jesusvillegas3.imperialdaycare.ProviderPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jesus Villegas 3 on 10/24/2016.
 */

public class AddProviderActivity extends AppCompatActivity {

    @BindView(R.id.provider_full_name_et)
    EditText providerFullName;
    @BindView(R.id.provider_id_et)
    EditText providerId;
    @BindView(R.id.provider_alt_id_et)
    EditText providerAltId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.provider_adder);
        ButterKnife.bind(this);

        setupExistingInfo();
    }

    private void setupExistingInfo() {
        ProviderPreferences.Preferences parentInfo = ProviderPreferences.getProviderInfo(this);
        if(parentInfo == null) return;
        providerFullName.setText(parentInfo.name);
        providerId.setText(parentInfo.key);
        providerAltId.setText(parentInfo.altId);
    }

    @OnClick(R.id.save_button)
    public void saveParent() {
        String fullName = providerFullName.getText().toString().trim();
        String id = providerId.getText().toString().trim();
        String altId = providerAltId.getText().toString().trim();

        if (fullName.isEmpty() || id.isEmpty()) {
            Toast.makeText(this, R.string.error_missing_fields, Toast.LENGTH_SHORT).show();
            return;
        }

        ProviderPreferences.setProviderInfo(fullName, id, altId, this);

        Toast.makeText(this, R.string.provider_add_success, Toast.LENGTH_SHORT).show();
        finish();
    }

}
