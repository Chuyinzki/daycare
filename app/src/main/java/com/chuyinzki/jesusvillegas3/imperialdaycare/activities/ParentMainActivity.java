package com.chuyinzki.jesusvillegas3.imperialdaycare.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

/**
 * Created by Jesus Villegas 3 on 11/22/2016.
 */

@SuppressLint("Registered")
public class ParentMainActivity extends AppCompatActivity {
    Toast toasty;
    String TAG = getClass().getName();
    protected InterstitialAd mInterstitialAd;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toasty = Toast.makeText(getApplicationContext(), "yolo", Toast.LENGTH_SHORT);

        AdRequest adRequest = new AdRequest.Builder().build();
        String actualAdID = "ca-app-pub-3654106040298396/1140134777";
        String testAdID = "ca-app-pub-3940256099942544/1033173712";
        InterstitialAd.load(this, actualAdID, adRequest,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd;
                        Log.i(TAG, "onAdLoaded");
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error
                        Log.i(TAG, loadAdError.getMessage());
                        mInterstitialAd = null;
                    }
                });
    }

    protected void toastLong(String message) {
        toasty.cancel();
        toasty.setDuration(Toast.LENGTH_LONG);
        toast(message);
    }

    protected void toastShort(String message) {
        toasty.cancel();
        toasty.setDuration(Toast.LENGTH_SHORT);
        toast(message);
    }

    private void toast(String message) {
        toasty.setText(message);
        toasty.show();
    }
}
