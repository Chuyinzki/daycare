package com.chuyinzki.jesusvillegas3.imperialdaycare.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.chuyinzki.jesusvillegas3.imperialdaycare.ChildDatabase;
import com.chuyinzki.jesusvillegas3.imperialdaycare.ProviderPreferences;
import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.Child;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jesus Villegas 3 on 10/24/2016.
 */

public class MainActivity extends AppCompatActivity {
    public static final String CHILD_EXTRA = "CHILD_EXTRA";
    public static final String RECORD_EXTRA = "RECORD_EXTRA";

    static final int UPDATE_CHILD = 100;
    static final int ADD_CHILD = 101;

    @BindView(R.id.daycare_children_lv)
    ListView children_lv;

    @BindView(R.id.inactive_daycare_children_lv)
    ListView inactive_children_lv;

    ChildAdapter adapter;
    ChildAdapter inactiveChildrenAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        ButterKnife.bind(this);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        List<Child> childrenList = ChildDatabase.getInstance(this).getChildren();
        List<Child> inactiveChildrenList = new LinkedList<>();

        Iterator<Child> iter = childrenList.iterator();
        while (iter.hasNext()) {
            Child c = iter.next();
            if (!c.getActiveStatus()) {
                iter.remove();
                inactiveChildrenList.add(c);
            }
        }

        adapter = new ChildAdapter(this, childrenList);
        inactiveChildrenAdapter = new ChildAdapter(this, inactiveChildrenList);

        children_lv.setAdapter(adapter);
        inactive_children_lv.setAdapter(inactiveChildrenAdapter);

        children_lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, ChildAdderActivity.class);
                intent.putExtra(CHILD_EXTRA, adapter.getItem(i));
                startActivityForResult(intent, UPDATE_CHILD);
                return true;
            }
        });
        inactive_children_lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, ChildAdderActivity.class);
                intent.putExtra(CHILD_EXTRA, inactiveChildrenAdapter.getItem(i));
                startActivityForResult(intent, UPDATE_CHILD);
                return true;
            }
        });
        children_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, CheckinCheckoutActivity.class);
                intent.putExtra(CHILD_EXTRA, adapter.getItem(i));
                startActivity(intent);
            }
        });
        inactive_children_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, CheckinCheckoutActivity.class);
                intent.putExtra(CHILD_EXTRA, inactiveChildrenAdapter.getItem(i));
                startActivity(intent);
            }
        });
        Button btn = new Button(this);
        btn.setText(R.string.go_add_child);
        children_lv.addFooterView(btn);
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivityForResult(new Intent(MainActivity.this, ChildAdderActivity.class), ADD_CHILD);
            }
        });
        sortListAndRedraw();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == RESULT_OK) {
                Child updatedChild = data.getParcelableExtra(ChildAdderActivity.CHILD_EXTRA_RESULT);
                if (requestCode == UPDATE_CHILD) {
                    adapter.remove(updatedChild);
                    inactiveChildrenAdapter.remove(updatedChild);
                }
                if (!data.getBooleanExtra(ChildAdderActivity.CHILD_EXTRA_DELETED, false)) {
                    if(updatedChild != null && updatedChild.getActiveStatus())
                        adapter.add(updatedChild);
                    else
                        inactiveChildrenAdapter.add(updatedChild);
                    sortListAndRedraw();
                }
            }
    }

    private void sortListAndRedraw() {
        Comparator<Child> comparator = new Comparator<Child>() {
            @Override
            public int compare(Child lhs, Child rhs) {
                return lhs.getFirstName().compareTo(rhs.getFirstName());
            }
        };
        adapter.sort(comparator);
        inactiveChildrenAdapter.sort(comparator);

        ListUtils.setDynamicHeight(children_lv);
        ListUtils.setDynamicHeight(inactive_children_lv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.edit_parent_info) {
            startActivity(new Intent(this, AddProviderActivity.class));
            return true;
        } else if (id == R.id.go_add_child) {
            startActivityForResult(new Intent(this, ChildAdderActivity.class), ADD_CHILD);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!ProviderPreferences.preferencesValid(this))
            startActivity(new Intent(this, AddProviderActivity.class));
    }

    public class ChildAdapter extends ArrayAdapter<Child> {

        public ChildAdapter(Context context, List<Child> list) {
            super(context, R.layout.child_lv_item, list);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            ViewHolder holder;
            if (view != null) {
                holder = (ViewHolder) view.getTag();
            } else {
                view = getLayoutInflater().inflate(R.layout.child_lv_item, null);
                holder = new ViewHolder(view);
                view.setTag(holder);
            }

            Child child = getItem(position);
            holder.text.setText(child.getFirstName() + " " + child.getLastName());
            return view;
        }

        class ViewHolder {
            @BindView(R.id.item_text)
            TextView text;

            ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
}
