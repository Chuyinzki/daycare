package com.chuyinzki.jesusvillegas3.imperialdaycare;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.Child;
import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.DayRecord;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static android.content.ContentValues.TAG;

/**
 * Created by Jesus Villegas 3 on 10/23/2016.
 */

public class ChildDatabase extends SQLiteOpenHelper {


    private static ChildDatabase sInstance;

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "Children.db";
    private static final String CHILD_TABLE_NAME = "children";

    private static final String PRIMARY_KEY_ID = "id";
    private static final String KEY_FIRST_NAME = "firstName";
    private static final String KEY_LAST_NAME = "lastName";
    private static final String KEY_FAMILY_ID = "familyId";
    private static final String KEY_CHILD_ID = "childId";
    private static final String KEY_DOB = "dateOfBirth";
    private static final String KEY_PARENT_NAME = "parentName";
    private static final String KEY_TECH_NUMBER = "techNumber";
    private static final String KEY_CASE_NUMBER = "caseNumber";
    private static final String KEY_ACTIVE_STATUS = "activeStatus";
    private static final String KEY_SUBTABLE_NAME = "subTableName";

    private static final String KEY_PRIMARY_TIME_ID = "time_id";
    private static final String KEY_CHECKIN = "checkin";
    private static final String KEY_CHECKOUT = "checkout";
    private static final String KEY_SCHOOL_CHECKIN = "school_checkin";
    private static final String KEY_SCHOOL_CHECKOUT = "school_checkout";

    private static final String CHILD_TABLE_CREATE =
            "CREATE TABLE " + CHILD_TABLE_NAME + " (" +
                    PRIMARY_KEY_ID + " INTEGER PRIMARY KEY, " +
                    KEY_FIRST_NAME + " TEXT, " +
                    KEY_LAST_NAME + " TEXT, " +
                    KEY_DOB + " INTEGER, " +
                    KEY_FAMILY_ID + " TEXT, " +
                    KEY_CHILD_ID + " TEXT, " +
                    KEY_PARENT_NAME + " TEXT, " +
                    KEY_TECH_NUMBER + " TEXT, " +
                    KEY_CASE_NUMBER + " TEXT, " +
                    KEY_ACTIVE_STATUS + " BOOLEAN, " +
                    KEY_SUBTABLE_NAME + " TEXT);";

    public static synchronized ChildDatabase getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null)
            sInstance = new ChildDatabase(context.getApplicationContext());
        return sInstance;
    }

    private ChildDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CHILD_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 1:
                //upgrade from version 1 to 2
                db.execSQL("ALTER TABLE " + CHILD_TABLE_NAME + " ADD COLUMN " + KEY_CASE_NUMBER +
                        " TEXT");
	    case 2:
                //upgrade from version 2 to 3
                db.execSQL("ALTER TABLE " + CHILD_TABLE_NAME + " ADD COLUMN " + KEY_ACTIVE_STATUS +
                        " BOOLEAN");
        }
    }

    public boolean insertChild(Child child) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PRIMARY_KEY_ID, child.getId());
        contentValues.put(KEY_FIRST_NAME, child.getFirstName().trim());
        contentValues.put(KEY_LAST_NAME, child.getLastName().trim());
        contentValues.put(KEY_DOB, child.getDob());
        contentValues.put(KEY_FAMILY_ID, child.getFamId().trim());
        contentValues.put(KEY_CHILD_ID, child.getChildId().trim());
        contentValues.put(KEY_PARENT_NAME, child.getParentName().trim());
        contentValues.put(KEY_TECH_NUMBER, child.getTechNumber().trim());
        contentValues.put(KEY_CASE_NUMBER, child.getCaseNumber().trim());
        contentValues.put(KEY_ACTIVE_STATUS, child.getActiveStatus());
        contentValues.put(KEY_SUBTABLE_NAME, createSubTableName(child));
        db.insert(CHILD_TABLE_NAME, null, contentValues);
        db.execSQL(getTimeTableCreate(createSubTableName(child)));
        printAllTableNames();
        return true;
    }

    public boolean updateChild(Child child) {
        //String currentTableName = getExistingChildTableName(child.getId());
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_FIRST_NAME, child.getFirstName().trim());
        contentValues.put(KEY_LAST_NAME, child.getLastName().trim());
        contentValues.put(KEY_DOB, child.getDob());
        contentValues.put(KEY_FAMILY_ID, child.getFamId().trim());
        contentValues.put(KEY_CHILD_ID, child.getChildId().trim());
        contentValues.put(KEY_PARENT_NAME, child.getParentName().trim());
        contentValues.put(KEY_TECH_NUMBER, child.getTechNumber().trim());
        contentValues.put(KEY_CASE_NUMBER, child.getCaseNumber().trim());
        contentValues.put(KEY_ACTIVE_STATUS, child.getActiveStatus());
        db.update(CHILD_TABLE_NAME, contentValues, PRIMARY_KEY_ID + "=" + child.getId(), null);
        printAllTableNames();
        return true;
    }

    public boolean deleteChild(Child child) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + getExistingChildSubTableName(child.getId()));
        db.delete(CHILD_TABLE_NAME, PRIMARY_KEY_ID + "=" + child.getId(), null);
        printAllTableNames();
        return true;
    }

    private String createSubTableName(String firstName, Long childId) {
        return firstName.replace(" ", "") + childId;
    }

    private String createSubTableName(Child child) {
        return createSubTableName(child.getFirstName().trim(), child.getId());
    }

    @SuppressLint("Range")
    private Long getTimeOnDate(Child child, String date, String key) {
        Cursor res = getChildSubTableCursor(child, date);
        Long ret = null;
        res.moveToFirst();

        if (res.getCount() != 0)
            ret = res.getLong(res.getColumnIndex(key));

        res.close();
        return ret == null || ret == 0 ? null : ret;
    }

    public Long getChildCheckinOnDate(Child child, String date) {
        return getTimeOnDate(child, date, KEY_CHECKIN);
    }

    public Long getChildCheckoutOnDate(Child child, String date) {
        return getTimeOnDate(child, date, KEY_CHECKOUT);
    }

    public Long getChildSchoolCheckinOnDate(Child child, String date) {
        return getTimeOnDate(child, date, KEY_SCHOOL_CHECKIN);
    }

    public Long getChildSchoolCheckoutOnDate(Child child, String date) {
        return getTimeOnDate(child, date, KEY_SCHOOL_CHECKOUT);
    }

    @Deprecated //Use insertOrUpdateDateTimes
    public long putChildCheckinOnDate(Child child, String date, Long timestamp) {
        return insertOrUpdateDateTimes(child, date, timestamp, getChildCheckoutOnDate(child, date),
                getChildSchoolCheckinOnDate(child, date), getChildSchoolCheckoutOnDate(child, date));
    }

    @Deprecated //Use insertOrUpdateDateTimes
    public long putChildCheckoutOnDate(Child child, String date, Long timestamp) {
        return insertOrUpdateDateTimes(child, date, getChildCheckinOnDate(child, date), timestamp,
                getChildSchoolCheckinOnDate(child, date), getChildSchoolCheckoutOnDate(child, date));
    }

    @Deprecated //Use insertOrUpdateDateTimes
    public long putChildSchoolCheckinOnDate(Child child, String date, Long timestamp) {
        return insertOrUpdateDateTimes(child, date,  getChildCheckinOnDate(child, date),
                getChildCheckoutOnDate(child, date), timestamp, getChildSchoolCheckoutOnDate(child, date));
    }

    @Deprecated //Use insertOrUpdateDateTimes
    public long putChildSchoolCheckoutOnDate(Child child, String date, Long timestamp) {
        return insertOrUpdateDateTimes(child, date, getChildCheckinOnDate(child, date),
                getChildCheckoutOnDate(child, date), getChildSchoolCheckinOnDate(child, date), timestamp);
    }

    public long insertOrUpdateDateTimes(Child child, DayRecord record) {
        return insertOrUpdateDateTimes(child, record.getOfficialFormattedDate(),
                record.getCheckIn() != null ? Long.parseLong(record.getCheckIn()) : null,
                record.getCheckOut() != null ? Long.parseLong(record.getCheckOut()) : null,
                record.getSchoolCheckIn() != null ? Long.parseLong(record.getSchoolCheckIn()) : null,
                record.getSchoolCheckOut() != null ? Long.parseLong(record.getSchoolCheckOut()) : null);
    }

    private long insertOrUpdateDateTimes(Child child, String date, Long checkinTime,
                                         Long checkoutTime, Long schoolCheckInTime,
                                         Long schoolCheckOutTime) {
        String subTableName = getExistingChildSubTableName(child.getId());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        Cursor res = getChildSubTableCursor(child, date);
        int count = res.getCount();
        res.close();
        if (count == 0) {
            contentValues.put(KEY_PRIMARY_TIME_ID, date);
            contentValues.put(KEY_CHECKIN, checkinTime);
            contentValues.put(KEY_CHECKOUT, checkoutTime);
            contentValues.put(KEY_SCHOOL_CHECKIN, schoolCheckInTime);
            contentValues.put(KEY_SCHOOL_CHECKOUT, schoolCheckOutTime);
            return db.insert(subTableName, null, contentValues);
        } else {
            contentValues.put(KEY_CHECKIN, checkinTime);
            contentValues.put(KEY_CHECKOUT, checkoutTime);
            contentValues.put(KEY_SCHOOL_CHECKIN, schoolCheckInTime);
            contentValues.put(KEY_SCHOOL_CHECKOUT, schoolCheckOutTime);
            return db.update(subTableName, contentValues, String.format("%s='%s'", KEY_PRIMARY_TIME_ID, date), null);
        }
    }

    public boolean deleteDateTime(Child child, DayRecord date){
        String subTableName = getExistingChildSubTableName(child.getId());
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(subTableName, KEY_PRIMARY_TIME_ID + "=?",
                new String[]{date.getOfficialFormattedDate()}) > 0;
    }

    @SuppressLint("Range")
    public List<DayRecord> getAllChildDayRecords(Child child) {
        ArrayList<DayRecord> records = new ArrayList<>();
        Cursor cur = getChildSubTableCursor(child);
        if (cur.moveToFirst()) {
            while (!cur.isAfterLast()) {
                records.add(new DayRecord(cur.getString(cur.getColumnIndex(KEY_PRIMARY_TIME_ID)),
                        cur.getString(cur.getColumnIndex(KEY_CHECKIN)),
                        cur.getString(cur.getColumnIndex(KEY_CHECKOUT)),
                        cur.getString(cur.getColumnIndex(KEY_SCHOOL_CHECKIN)),
                        cur.getString(cur.getColumnIndex(KEY_SCHOOL_CHECKOUT))
                ));
                cur.moveToNext();
            }
        }
        cur.close();
        return records;
    }

    private int getMonthIntegerOfDate(String timeID) {
        return Integer.parseInt(timeID.substring(0, 2)) - 1;
    }

    private int getYearIntegerOfDate(String timeID) {
        return Integer.parseInt(timeID.substring(4));
    }

    public List<DayRecord> getChildDayRecordsForMonth(Child child, int month, int year) {
        List<DayRecord> records = getAllChildDayRecords(child);
        ListIterator<DayRecord> iterator = records.listIterator();
        while (iterator.hasNext()) {
            String timeID = iterator.next().getTimeID();
            if (month != getMonthIntegerOfDate(timeID) || year != getYearIntegerOfDate(timeID))
                iterator.remove();
        }
        return records;
    }

    private Cursor getChildSubTableCursor(Child child, String date) {
        return this.getReadableDatabase().rawQuery("select * from " + getExistingChildSubTableName(child.getId()) + " where " +
                KEY_PRIMARY_TIME_ID + "='" + date + "'", null);
    }

    private Cursor getChildSubTableCursor(Child child) {
        return this.getReadableDatabase().rawQuery("select * from " + getExistingChildSubTableName(child.getId()), null);
    }

    //key format will be MMDDYYYY
    private String getTimeTableCreate(String tableName) {
        return "CREATE TABLE " + tableName + " (" +
                KEY_PRIMARY_TIME_ID + " TEXT PRIMARY KEY, " +
                KEY_CHECKIN + " INTEGER, " +
                KEY_CHECKOUT + " INTEGER, " +
                KEY_SCHOOL_CHECKIN + " INTEGER, " +
                KEY_SCHOOL_CHECKOUT + " INTEGER);";
    }

    private void printAllTableNames() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                Log.d(ChildDatabase.class.getSimpleName(), "Table Name=> " + c.getString(0));
                c.moveToNext();
            }
        }
        c.close();
    }

    @SuppressLint("Range")
    public void logChildren() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + CHILD_TABLE_NAME, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            Log.d(ChildDatabase.class.getSimpleName(), String.format("%s, %s, %s, %s, %s, %s, %s, %s, %s, %B",
                    res.getLong(res.getColumnIndex(PRIMARY_KEY_ID)),
                    res.getString(res.getColumnIndex(KEY_FIRST_NAME)),
                    res.getString(res.getColumnIndex(KEY_LAST_NAME)),
                    new Timestamp(res.getLong(res.getColumnIndex(KEY_DOB))),
                    res.getString(res.getColumnIndex(KEY_FAMILY_ID)),
                    res.getString(res.getColumnIndex(KEY_CHILD_ID)),
                    res.getString(res.getColumnIndex(KEY_PARENT_NAME)),
                    res.getString(res.getColumnIndex(KEY_TECH_NUMBER)),
                    res.getString(res.getColumnIndex(KEY_CASE_NUMBER)),
                    res.getInt(res.getColumnIndex(KEY_ACTIVE_STATUS)) > 0

            ));
            res.moveToNext();
        }
        res.close();
    }

    private String getExistingChildSubTableName(Long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + CHILD_TABLE_NAME + " where " + PRIMARY_KEY_ID + "=" + id, null);

        res.moveToFirst();
        @SuppressLint("Range") String ret = res.getString(res.getColumnIndex(KEY_SUBTABLE_NAME));
        res.close();
        return ret;
    }

    public boolean childIdExists(Child child) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("select * from " + CHILD_TABLE_NAME + " WHERE " +
                KEY_CHILD_ID + "=" + child.getChildId(), null);
        cur.moveToFirst();

        @SuppressLint("Range") boolean ret = !(cur.getCount() == 0 ||
                (child.getId() != null && cur.getLong(cur.getColumnIndex(PRIMARY_KEY_ID)) == child.getId()));
        cur.close();
        return ret;
    }

    @SuppressLint("Range")
    public ArrayList<Child> getChildren() {
        ArrayList<Child> ret = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + CHILD_TABLE_NAME, null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            ret.add(new Child(res.getLong(res.getColumnIndex(PRIMARY_KEY_ID)),
                    res.getString(res.getColumnIndex(KEY_FIRST_NAME)),
                    res.getString(res.getColumnIndex(KEY_LAST_NAME)),
                    res.getString(res.getColumnIndex(KEY_FAMILY_ID)),
                    res.getString(res.getColumnIndex(KEY_CHILD_ID)),
                    res.getString(res.getColumnIndex(KEY_PARENT_NAME)),
                    res.getString(res.getColumnIndex(KEY_TECH_NUMBER)),
                    res.getString(res.getColumnIndex(KEY_CASE_NUMBER)),
                    res.getInt(res.getColumnIndex(KEY_ACTIVE_STATUS)) > 0,
                    res.getLong(res.getColumnIndex(KEY_DOB))
            ));
            res.moveToNext();
        }
        res.close();
        return ret;
    }

    @SuppressLint("Range")
    public String getTableAsString(SQLiteDatabase db, String tableName) {
        Log.d(TAG, "getTableAsString called");
        String tableString = String.format("Table %s:\n", tableName);
        Cursor allRows = db.rawQuery("SELECT * FROM " + tableName, null);
        if (allRows.moveToFirst()) {
            String[] columnNames = allRows.getColumnNames();
            do {
                for (String name : columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)));
                }
                tableString += "\n";

            } while (allRows.moveToNext());
        }

        return tableString;
    }
}
