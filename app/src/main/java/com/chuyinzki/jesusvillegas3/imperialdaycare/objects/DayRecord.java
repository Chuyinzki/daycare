package com.chuyinzki.jesusvillegas3.imperialdaycare.objects;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import java.time.YearMonth;
import java.util.Calendar;

/**
 * Created by Jesus Villegas 3 on 3/13/2018.
 */

public class DayRecord implements Parcelable, Comparable<DayRecord> {
    private String timeID;
    private String checkIn;
    private String checkOut;
    private String schoolCheckIn;
    private String schoolCheckOut;

    public DayRecord(String timeID) {
        this(timeID, null, null, null, null);
    }
    public DayRecord(String timeID, String checkIn, String checkOut, String schoolCheckIn, String schoolCheckOut) {
        this.timeID = timeID;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.schoolCheckIn = schoolCheckIn;
        this.schoolCheckOut = schoolCheckOut;
    }

    public boolean isCurrentlyValid() {
        //TODO: Add error checking to make sure checkout date is inside the timeID
        if((checkIn == null && checkOut != null) || (schoolCheckIn == null && schoolCheckOut != null) || timeID == null)
            return false;

        if(checkIn != null && checkOut != null && Long.parseLong(checkOut) < Long.parseLong(checkIn))
            return false;

        if(checkIn != null && checkOut != null && schoolCheckIn != null && schoolCheckOut != null) {
            return Long.parseLong(checkIn) <= Long.parseLong(schoolCheckOut) &&
                    Long.parseLong(schoolCheckOut) <= Long.parseLong(schoolCheckIn) &&
                    Long.parseLong(schoolCheckIn) <= Long.parseLong(checkOut);
        }
        return true;
    }

    public boolean isCompleteAndValid() {
        return isCurrentlyValid() && checkIn != null && checkOut != null &&
                ((schoolCheckIn == null && schoolCheckOut == null) || hasSchoolHours());
    }

    public boolean hasSchoolHours() {
        return schoolCheckIn != null && schoolCheckOut != null;
    }

    public String getTimeID() {
        return timeID;
    }

    public void setTimeID(String timeID) {
        if(timeID.length() != 8) {
            //TODO: invalid length, not saving
            return;
        }
        this.timeID = timeID;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public void setCheckIn(long checkIn) {
        this.checkIn = String.valueOf(checkIn);
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public void setCheckOut(long checkOut) {
        this.checkOut = String.valueOf(checkOut);
    }

    public String getSchoolCheckIn() {
        return schoolCheckIn;
    }

    public void setSchoolCheckIn(String schoolCheckIn) {
        this.schoolCheckIn = schoolCheckIn;
    }

    public void setSchoolCheckIn(long schoolCheckIn) {
        this.schoolCheckIn = String.valueOf(schoolCheckIn);
    }

    public String getSchoolCheckOut() {
        return schoolCheckOut;
    }

    public void setSchoolCheckOut(String schoolCheckOut) {
        this.schoolCheckOut = schoolCheckOut;
    }

    public void setSchoolCheckOut(long schoolCheckOut) {
        this.schoolCheckOut = String.valueOf(schoolCheckOut);
    }

    public String getFriendlyFormattedDate() {
        if(timeID == null)
            return "No Date";
        return getMonth() + "/" + getDay() + "/" + getFriendlyYear();
    }

    public String getFriendlyFormattedDateFullYear() {
        if(timeID == null)
            return "No Date";
        return getMonth() + "/" + getDay() + "/" + getFullYear();
    }

    public String getOfficialFormattedDate() {
        if(timeID == null)
            return "No Date";
        return getMonth() + getDay() +  getFullYear();
    }

    public String getFriendlyYear() {
        if(timeID == null)
            return "No Year";
        return timeID.substring(6);
    }

    public String getFullYear() {
        if(timeID == null)
            return "No Year";
        return timeID.substring(4);
    }

    public String getMonth() {
        if(timeID == null)
            return "No Month";
        return timeID.substring(0, 2);
    }

    public String getDay() {
        if(timeID == null)
            return "No Day";
        return timeID.substring(2, 4);
    }

    public Calendar getDayAsCalendar() {
        Calendar ret = Calendar.getInstance();
        ret.set(Integer.parseInt(getFullYear()), Integer.parseInt(getMonth()) - 1, Integer.parseInt(getDay()));
        return ret;
    }

    public int getDaysInCurrentMonth() {
        return getDayAsCalendar().getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    protected DayRecord(Parcel in) {
        timeID = in.readString();
        checkIn = in.readString();
        checkOut = in.readString();
        schoolCheckIn = in.readString();
        schoolCheckOut = in.readString();
    }

    public static final Creator<DayRecord> CREATOR = new Creator<DayRecord>() {
        @Override
        public DayRecord createFromParcel(Parcel in) {
            return new DayRecord(in);
        }

        @Override
        public DayRecord[] newArray(int size) {
            return new DayRecord[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(timeID);
        dest.writeString(checkIn);
        dest.writeString(checkOut);
        dest.writeString(schoolCheckIn);
        dest.writeString(schoolCheckOut);
    }

    @Override
    public int compareTo(DayRecord o) {
        return getDayAsCalendar().compareTo(o.getDayAsCalendar());
    }
}
