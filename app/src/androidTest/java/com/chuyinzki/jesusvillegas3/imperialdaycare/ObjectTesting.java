package com.chuyinzki.jesusvillegas3.imperialdaycare;

import android.content.Context;
import android.util.Pair;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.runner.AndroidJUnit4;

import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.Child;
import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.DayRecord;
import com.chuyinzki.jesusvillegas3.imperialdaycare.objects.Util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ObjectTesting {
    private Context appContext;
    private Child newChild;
    private boolean result;

    private final String FIRST_NAME = "Jesus";
    private final String LAST_NAME = "Villegas";
    private final String FAM_ID = "123456";
    private final String CHILD_ID = "432101";
    private final String TECH_NUMBER = "Johnson";
    private final String PARENT_NAME = "Georgina Villegas";
    private final String CASE_NUMBER = "123456";
    private final Calendar CAL = Calendar.getInstance();
    private final int BIRTH_YEAR = 1992;
    private final int BIRTH_MONTH = Calendar.JULY;
    private final int BIRTH_DATE = 7;

    private final int RECORDS_YEAR = 2020;
    private final int RECORDS_MONTH = Calendar.APRIL;
    private Calendar debugCalendar = Calendar.getInstance();

    @Before
    public void setup() {
        appContext = ApplicationProvider.getApplicationContext();
        CAL.set(BIRTH_YEAR, BIRTH_MONTH, BIRTH_DATE);
        newChild = new Child(Calendar.getInstance().getTimeInMillis(), FIRST_NAME, LAST_NAME,
                FAM_ID, CHILD_ID, PARENT_NAME, TECH_NUMBER , CASE_NUMBER, true, CAL.getTimeInMillis());
        result = ChildDatabase.getInstance(appContext).insertChild(newChild);

        debugCalendar.clear();
        debugCalendar.set(RECORDS_YEAR, RECORDS_MONTH, 15);
    }

    @Test
    public void currentDataFill() {
        Calendar curCal = Calendar.getInstance();
        curCal.set(Calendar.MONTH, curCal.get(Calendar.MONTH) - 1);
        setDebugHours(newChild, curCal);
    }

    @Test
    public void useAppContext() {
        assertEquals("com.chuyinzki.jesusvillegas3.imperialdaycare", appContext.getPackageName());
    }

    @Test
    public void createChildTest() {
        assertTrue(result);
    }

    @Test
    public void getChildTest() {
        ArrayList<Child> children = ChildDatabase.getInstance(appContext).getChildren();
        assertTrue(children.contains(newChild));
    }

    @Test
    public void childEquivalenceTest() {
        ArrayList<Child> children = ChildDatabase.getInstance(appContext).getChildren();
        Child child = children.get(children.indexOf(newChild));
        assertEquals(FIRST_NAME, child.getFirstName());
        assertEquals(LAST_NAME, child.getLastName());
        assertEquals(FAM_ID, child.getFamId());
        assertEquals(CHILD_ID, child.getChildId());
        assertEquals(PARENT_NAME, child.getParentName());

        Calendar curCalendar = Calendar.getInstance();
        curCalendar.setTimeInMillis(child.getDob());
        assertEquals(BIRTH_YEAR, curCalendar.get(Calendar.YEAR));
        assertEquals(BIRTH_MONTH, curCalendar.get(Calendar.MONTH));
        assertEquals(BIRTH_DATE, curCalendar.get(Calendar.DATE));
    }

    @Test
    public void recordsTest() {
        setDebugHours(newChild, debugCalendar);
        List<DayRecord> records = getRecords();
        assertEquals(22, records.size());

        List<DayRecord> week1Records = Util.getRecordsForWeekNumber(records, 1);
        assertEquals(3, week1Records.size());
        List<DayRecord> week2Records = Util.getRecordsForWeekNumber(records, 2);
        assertEquals(5, week2Records.size());
        List<DayRecord> week3Records = Util.getRecordsForWeekNumber(records, 3);
        assertEquals(5, week3Records.size());
        List<DayRecord> week4Records = Util.getRecordsForWeekNumber(records, 4);
        assertEquals(5, week4Records.size());
        List<DayRecord> week5Records = Util.getRecordsForWeekNumber(records, 5);
        assertEquals(4, week5Records.size());

        DayRecord firstDayRecord = records.get(0);
        Pair<Float, Float> amPmTimes = Util.getAmPmHours(firstDayRecord);
        assertEquals(10.0, amPmTimes.first, 1e-15);
        assertEquals(2.0, amPmTimes.second, 1e-15);

        Pair<Float, Float> week1AmPmHours = Util.getAmPmHoursForWeek(records, 1);
        assertEquals(30.0, week1AmPmHours.first, 1e-15);
        assertEquals(6.0, week1AmPmHours.second, 1e-15);

        Pair<Float, Float> week2AmPmHours = Util.getAmPmHoursForWeek(records, 2);
        assertEquals(50.0, week2AmPmHours.first, 1e-15);
        assertEquals(10.0, week2AmPmHours.second, 1e-15);

        Pair<Float, Float> week5AmPmHours = Util.getAmPmHoursForWeek(records, 5);
        assertEquals(40.0, week5AmPmHours.first, 1e-15);
        assertEquals(8.0, week5AmPmHours.second, 1e-15);
    }

    @Test
    public void dayRecordValidityTests() {
        debugCalendar.set(Calendar.HOUR_OF_DAY, 6);
        Long am6 = debugCalendar.getTimeInMillis();

        debugCalendar.set(Calendar.HOUR_OF_DAY, 9);
        Long am9 = debugCalendar.getTimeInMillis();

        debugCalendar.set(Calendar.HOUR_OF_DAY, 13);
        Long pm1 = debugCalendar.getTimeInMillis();

        debugCalendar.set(Calendar.HOUR_OF_DAY, 16);
        Long pm4 = debugCalendar.getTimeInMillis();

        DayRecord record = new DayRecord("02022020", String.valueOf(am6), String.valueOf(pm1), null, null);
        assertTrue(record.isCompleteAndValid());

        record = new DayRecord("02022020", String.valueOf(pm1), String.valueOf(am6), null, null);
        assertFalse(record.isCompleteAndValid());

        Utils.debugAddValuesForMonth(newChild, appContext, Util.getMonthDayYear(null, debugCalendar.getTime()),
                5, 12, 15, 22);
        List<DayRecord> records = getRecords();

        DayRecord firstDayRecord = records.get(0);
        Pair<Float, Float> amPmTimes = Util.getAmPmHours(firstDayRecord);
        assertEquals(9.0, amPmTimes.first, 1e-15);
        assertEquals(5.0, amPmTimes.second, 1e-15);

        System.out.println();
    }

    @Test
    public void fillOutMissingRecordsTest() {
        setDebugHours(newChild, debugCalendar);
        List<DayRecord> records = getRecords();
        assertEquals(22, records.size());
        Util.fillOutEmptyRecords(records);
        assertEquals(30, records.size());
    }

    private void setDebugHours(Child child, Calendar calendar) {
        Utils.debugAddValuesForMonth(child, appContext, Util.getMonthDayYear(null, calendar.getTime()), 8, null, null, 20);
    }

    private List<DayRecord> getRecords() {
        ArrayList<Child> children = ChildDatabase.getInstance(appContext).getChildren();
        Child child = children.get(children.indexOf(newChild));
        return ChildDatabase.getInstance(appContext)
                .getChildDayRecordsForMonth(child, RECORDS_MONTH, RECORDS_YEAR);
    }
}
